export interface IGame {
  id?: number;
  title: string;
  description: string;
  type: string;
  image: string;
  users: number;
  worth: string;
  platforms: string;
  status: string;
  gamerpower_url: string;
  instructions: string;
  open_giveaway: string;
  open_giveaway_url: string;
  published_date: string;
  end_date: string;
  thumbnail: string;
}
