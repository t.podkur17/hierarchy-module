import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, delay, Observable, retry, throwError } from 'rxjs';
import { ErrorService } from './error.service';
import { IGame } from '../models/game';

@Injectable({
  providedIn: 'root',
})
export class GamesService {
  headers = {
    'X-RapidAPI-Key': '2324da9e22mshe432a11b475f24cp11d700jsn4bd004bddebc',
    'X-RapidAPI-Host': 'gamerpower.p.rapidapi.com',
  };

  constructor(private http: HttpClient, private errorService: ErrorService) {}

  getAll(): Observable<IGame[]> {
    return this.http
      .get<IGame[]>('https://gamerpower.p.rapidapi.com/api/giveaways', {
        headers: this.headers,
      })
      .pipe(delay(200), retry(2), catchError(this.errorHandler.bind(this)));
  }

  getGameById(id: string): Observable<IGame> {
    return this.http
      .get<IGame>('https://gamerpower.p.rapidapi.com/api/giveaway?id=' + id, {
        headers: this.headers,
      })
      .pipe(delay(200), retry(2), catchError(this.errorHandler.bind(this)));
  }

  private errorHandler(error: HttpErrorResponse) {
    this.errorService.handle(error.message);
    return throwError(() => error.message);
  }
}
