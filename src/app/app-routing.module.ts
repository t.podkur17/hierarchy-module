import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { GameDetailsPageComponent } from './pages/game-details-page/game-details-page.component';
import { NavigationPath } from '@app/core/navigation/common';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: NavigationPath.GameDetails, component: GameDetailsPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
