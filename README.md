# Hierarchy module

## About The Project

In view of the development of distance education on the Internet, a large number of educational electronic platforms are appearing. To help students find educational content, we are developing the ChooseYourCourse recommendation system. It includes courses from several platforms simultaneously and builds a unified classification for them, but the result of recommendations has no structure and does not show the order of study of educational materials. Therefore, the task of developing algorithms to build a hierarchy of courses for the recommendation system of distance learning is relevant.

This project provides a software module for building a hierarchy of learning materials of the ChooseYourCourse recommendation system. 
The program aggregates books and articles from electronic libraries and builds a hierarchy of training materials. The program forms a database for the recommendation system.

The database obtained as a result of the work of the created module can be used by developers in the implementation of the course recommendation system. It contains data on the hierarchy of courses, which can be the basis for compiling user recommendations.


### Built With

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.4.
[![Angular][Angular.io]][Angular-url]


## Getting Started

### Prerequisites

Before running the application, you will need to install [Node.js and NPM package manager](https://nodejs.org/en/download).

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/t.podkur17/hierarchy-module.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Run a dev server
   ```js
   ng serve
   ```
4. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.
